class Card {
    constructor(post) {
        this.post = post;
        this.cardElement = this.createCardElement();
    }

    createCardElement() {
        const { title, text, user } = this.post;

        const card = document.createElement('div');
        card.classList.add('card');

        const cardTitle = document.createElement('h3');cardTitle.textContent = title;

        const cardText = document.createElement('p');
        cardText.textContent = text;

        const userInfo = document.createElement('div');
        userInfo.textContent = `${user.name} ${user.surname} - ${user.email}`;

        const deleteButton = document.createElement('button');
        deleteButton.innerHTML = '&#10006;';
        deleteButton.classList.add('delete-button');
        deleteButton.addEventListener('click', this.deleteCard.bind(this));

        card.appendChild(cardTitle);
        card.appendChild(cardText);
        card.appendChild(userInfo);
        card.appendChild(deleteButton);

        return card;
    }

    deleteCard() {
        const {id} = this.post;
        const url = `https://ajax.test-danit.com/api/json/posts/${id}`;

        fetch(url, {method: 'DELETE'})
        .then(response => {
            if (Response.ok) {
                this.cardElement.remove();
            } else {
                throw new Error('Failed to delete card');
            }
        })
        .catch(error => {
            console.error(error);
        });
    }
}

function displayNewsFeed() {
    const usersUrl = 'https://ajax.test-danit.com/api/json/users';
    const postsUrl = 'https://ajax.test-danit.com/api/json/posts';
    const root = document.getElementById('root');

    Promise.all([fetch(usersUrl), fetch(postsUrl)])
    .then(([usersResponse, postsResponse]) => Promise.all([usersResponse.json(), postsResponse.json()]))
    .then(([users, posts]) => {
        posts.forEach(post => {
            const currentUser = users.find(user => user.id === post.userId);
            if(currentUser) {
                const card = new Card({
                    id: post.id,
                    title: post.title,
                    text: post.body,
                    user: currentUser
                });
                root.appendChild(card.cardElement);
            }
        });
    })
    .catch(error => {
        console.error(error);
    });
}

displayNewsFeed();
